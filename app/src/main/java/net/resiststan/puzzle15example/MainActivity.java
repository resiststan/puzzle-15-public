package net.resiststan.puzzle15example;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.resiststan.puzzle15example.R;

import net.resiststan.puzzle15example.score.IStatisticsDAO;
import net.resiststan.puzzle15example.score.StatisticsImpl;
import net.resiststan.puzzle15example.util.android.CustomAppCompatActivity;
import net.resiststan.puzzle15example.util.data.Manager;

import java.io.Serializable;

import static net.resiststan.puzzle15example.util.ui.ScoreBoard.scoreBoard;

public class MainActivity extends CustomAppCompatActivity {

    private Spinner spinner;
    private SharedPreferences prefs;

    @Override
    protected void onResume() {
        canResumeGame();

        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = this.getSharedPreferences(getString(R.string.app_pref), Context.MODE_PRIVATE);

        setupSpinner();
        setupStartBtn();
        setupResumeBtn();
        setupScoreBtn();
        setupAboutBtn();
        setupSettingsBtn();
        setupExitBtn();
    }

    private Serializable getSavedGame() {
        return (Serializable) Manager.readObj(this, getString(R.string.game_data));
    }

    private void setupSpinner() {
        spinner = findViewById(R.id.spinnerFieldSide);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter
                .createFromResource(
                        this,
                        R.array.field_side_entries,
                        android.R.layout.simple_spinner_item
                );

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection((int) loadSelectedItem());
    }

    private void setupStartBtn() {
        Button btn = findViewById(R.id.btnStart);
        btn.setOnClickListener(v -> {
            String[] gateType = getResources().getStringArray(R.array.game_type);
            Intent intent = GameActivity.makeIntentNewGame(MainActivity.this, (int) spinner.getSelectedItemId(), gateType[0]);
            startActivity(intent);
        });
    }

    private void setupResumeBtn() {
        Button btn = findViewById(R.id.btnResume);
        canResumeGame();
        btn.setOnClickListener(v -> {

            Intent intent = GameActivity.makeIntentResumeGame(MainActivity.this, getSavedGame());
            startActivity(intent);
        });
    }

    private void canResumeGame() {
        Button btn = findViewById(R.id.btnResume);
        btn.setEnabled(getSavedGame() != null);
    }

    private void setupScoreBtn() {
        Button btn = findViewById(R.id.btnScore);
        btn.setOnClickListener(v -> {

            IStatisticsDAO statistics = new StatisticsImpl(this);

            //Задел на красивую форму счета
            /*AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            View view = getLayoutInflater().inflate(R.layout.dialog_score, null);
            dialog.setView(view);
            AlertDialog d = dialog.create();
            d.show();*/

            new AlertDialog.Builder(this)
                    .setTitle(R.string.score)
                    .setMessage(scoreBoard(statistics, this))
                    .setNegativeButton(R.string.reset, (dialog, which) -> {
                        statistics.clearData();
                        btn.callOnClick();
                    })
                    .setPositiveButton(R.string.close, (dialog, which) -> {
                        //do nothing
                    })
                    .setCancelable(true)
                    .show();
        });
    }

    private void setupAboutBtn() {
        Button btn = findViewById(R.id.btnAbout);
        btn.setOnClickListener(v -> {
            Log.d(TAG, "pressed About Button");

            new AlertDialog.Builder(this)
                    .setTitle(R.string.about_dialog_title)
                    .setMessage(R.string.about_dialog_text)
                    .setNegativeButton(R.string.close, (dialog, which) -> {
                        //do nothing
                    })
                    .setCancelable(true)
                    .show();
        });
    }

    private void setupSettingsBtn() {
        Button btn = findViewById(R.id.btnSettings);
        btn.setOnClickListener(v -> {
            Log.d(TAG, "pressed Settings Button");

            android.support.v7.preference.PreferenceManager
                    .setDefaultValues(this,
                            R.xml.preferences, false);

            startActivity(new Intent(this, SettingsActivity.class));
        });
    }

    private void setupExitBtn() {
        Button btn = findViewById(R.id.btnExit);
        btn.setOnClickListener(v -> finish());
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "main on pause");

        saveSelectedItem();

        super.onPause();
    }

    private void saveSelectedItem() {
        prefs.edit().putLong(getString(R.string.game_field_side), spinner.getSelectedItemId()).apply();
    }

    private long loadSelectedItem() {
        return prefs.getLong(getString(R.string.game_field_side), 0L);
    }

}
