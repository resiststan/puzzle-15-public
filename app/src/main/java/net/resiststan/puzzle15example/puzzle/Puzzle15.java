package net.resiststan.puzzle15example.puzzle;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;

public class Puzzle15 implements Serializable {
    // field side
    private final static int MIN_SIDE = 2;
    private final static int STANDARD_SIDE = 4;
    private final static int MAX_SIDE = 10;

    private final int[] actualArr;
    private final int[] targetArr;
    private final int cellCount;
    private final int fieldSide;
    private final int oddSwapping;
    private int actualEmptyCell;

    public Puzzle15() {
        this(STANDARD_SIDE);
    }

    public Puzzle15(int fieldSide) {
        if (fieldSide <= MAX_SIDE && fieldSide >= MIN_SIDE) {
            this.fieldSide = fieldSide;
        } else {
            this.fieldSide = STANDARD_SIDE;
        }

        cellCount = this.fieldSide * this.fieldSide;
        targetArr = createTargetArr();
        oddSwapping = defineTargetOddSwapping();
        actualArr = Arrays.copyOf(targetArr, targetArr.length);
        startNewGame();
    }

    public int getActualEmptyCell() {
        return actualEmptyCell;
    }

    public int[] getTargetArray() {
        return targetArr;
    }

    public int[] getActualArray() {
        return actualArr;
    }

    public int getFieldSide() {
        return fieldSide;
    }

    public int getCellCount() {
        return cellCount;
    }

    private int defineTargetOddSwapping() {
        return definedAddSwapping(targetArr, targetArr.length - 1) % 2;
    }

    private int[] createTargetArr() {
        int[] arr = new int[cellCount];

        for (int i = 0; i < cellCount - 1; i++) {
            arr[i] = i + 1;
        }

        return arr;
    }

    private void setActualEmptyCell(int actualEmptyCell) {
        this.actualEmptyCell = actualEmptyCell;
    }

    private void swap(int i1, int i2) {
        int tmp = actualArr[i1];
        actualArr[i1] = actualArr[i2];
        actualArr[i2] = tmp;
    }

    private void definedEmptyCell() {
        for (int i = 0; i < cellCount; i++) {
            if (actualArr[i] == 0) {
                setActualEmptyCell(i);
                return;
            }
        }
    }

    private boolean isSwapped(int i) {
        return i > -1 && i < cellCount
                &&
                (i - fieldSide == actualEmptyCell
                        || i + fieldSide == actualEmptyCell
                        || i - 1 == actualEmptyCell && i % fieldSide != 0
                        || i + 1 == actualEmptyCell && i % fieldSide != fieldSide - 1)
                ;
    }

    private boolean isSolvable() {
        return definedAddSwapping(actualArr, getActualEmptyCell()) % 2 == oddSwapping;
    }

    private int definedAddSwapping(int[] arr, final int emptyCell) {
        int s = 0;

        for (int i = 0; i < cellCount; i++) {
            if (i == emptyCell) {
                continue;
            }

            if (isReverseMove(i)) {
                int m = (i % fieldSide) - (fieldSide - 1);

                for (int j = cellCount - 1; j > i - m; j--) {
                    if (j != emptyCell && arr[i] > arr[j]) {
                        s++;
                    }
                }

                m = i - (i % fieldSide);

                for (int j = m; j < i; j++) {
                    if (j != emptyCell && arr[i] > arr[j]) {
                        s++;
                    }
                }
            } else {
                for (int j = i + 1; j < cellCount; j++) {
                    if (j != emptyCell && arr[i] > arr[j]) {
                        s++;
                    }
                }
            }
        }

        return s;
    }

    private boolean isReverseMove(int i) {
        int c = 0;
        while ((i -= fieldSide) >= 0) {
            ++c;
        }

        return c % 2 == 1;// + fieldSide % 2;
    }


    public void startNewGame() {
        Random rnd = new Random();

        do {
            for (int i = cellCount; i > 1; i--) {
                swap(i - 1, rnd.nextInt(i));
            }

            definedEmptyCell();
        } while (!isSolvable());
    }

    public boolean isDecided() {
        return Arrays.equals(actualArr, getTargetArray());
    }

    public int moveCell(int i) {
        if (isSwapped(i)) {
            int oldEmpty = actualEmptyCell;
            swap(getActualEmptyCell(), i);
            setActualEmptyCell(i);

            return oldEmpty;
        }

        return -1;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < cellCount; i++) {
            str.append("[").append(i).append("]=").append(actualArr[i]).append(" ");

            if (i % fieldSide == fieldSide - 1) {
                str.append("\n");
            }
        }

        return str.toString();
    }
}
