package net.resiststan.puzzle15example.presenter;

import android.support.annotation.NonNull;

import net.resiststan.puzzle15example.contract.IContract;
import net.resiststan.puzzle15example.puzzle.Puzzle15;
import net.resiststan.puzzle15example.score.GameKey;
import net.resiststan.puzzle15example.score.GameScore;
import net.resiststan.puzzle15example.score.IStatisticsDAO;
import net.resiststan.puzzle15example.score.StatisticsImpl;

import java.io.Serializable;

public class GameFieldPresenter implements IContract.IGame.IPresenter, Serializable {
    private final String TAG = this.getClass().getSimpleName();

    private final Puzzle15 game;
    private final String fieldType;
    private final GameKey key;
    private transient IContract.IGame.IView view;
    private transient IStatisticsDAO statistics;
    private GameScore score;

    public GameFieldPresenter(int size, String fieldType) {
        this.fieldType = fieldType;
        this.game = new Puzzle15(size);
        this.key = new GameKey(fieldType, game.getFieldSide());
    }

    @Override
    public void attachView(IContract.IGame.IView view) {
        this.view = view;
        this.statistics = new StatisticsImpl(view.getContext());

        if (score == null) {
            score = statistics.getOne(key);

            if (score == null) {
                score = new GameScore(key);
            }
        }
    }

    @Override
    public void viewIsReady() {
        loadField();
        loadScoreboard();
    }

    @Override
    public void startNewGame() {
        score = statistics.getOne(key);
        game.startNewGame();
        viewIsReady();
    }

    @Override
    public void moveCell(int cellInd) {
        int oldEmptyCellInd = game.moveCell(cellInd);

        if (oldEmptyCellInd != -1) {
            view.fillCell(oldEmptyCellInd, String.valueOf(game.getActualArray()[oldEmptyCellInd]));
            view.clearCell(cellInd);

            view.updateScoreboard(score.increase());
        }

        if (game.isDecided()) {
            statistics.update(score);
            view.gameCompleted();
        }
    }

    @Override
    public void onRightToLeftSwipe() {
        moveCell(game.getActualEmptyCell() + 1);
    }

    @Override
    public void onLeftToRightSwipe() {
        moveCell(game.getActualEmptyCell() - 1);
    }

    @Override
    public void onTopToBottomSwipe() {
        moveCell(game.getActualEmptyCell() - game.getFieldSide());
    }

    @Override
    public void onBottomToTopSwipe() {
        moveCell(game.getActualEmptyCell() + game.getFieldSide());
    }

    @Override
    public int getFieldSide() {
        return game.getFieldSide();
    }

    @Override
    public @NonNull
    String getFieldType() {
        return fieldType;
    }

    private void loadField() {
        int[] arr = game.getActualArray();

        for (int i = 0; i < arr.length; i++) {
            if (i != game.getActualEmptyCell()) {
                view.fillCell(i, String.valueOf(game.getActualArray()[i]));
            } else {
                view.clearCell(i);
            }
        }
    }

    private void loadScoreboard() {
        view.setScoreboard(score.getBest(), score.getCurrent());
    }
}
