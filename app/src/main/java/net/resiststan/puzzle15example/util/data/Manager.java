package net.resiststan.puzzle15example.util.data;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Manager {

    public static void saveFile(Context context, String fileName, Serializable ser) {
        deleteFile(context, fileName);

        try (
                final FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
                final ObjectOutputStream os = new ObjectOutputStream(fos)
        ) {
            os.writeObject(ser);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static Object readObj(Context context, String fileName) {
        Object obj = null;

        try (
                final FileInputStream fis = context.openFileInput(fileName);
                final ObjectInputStream is = new ObjectInputStream(fis)
        ) {
            obj = is.readObject();
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return obj;

    }

    public static boolean deleteFile(Context context, String fileName) {
        return context.deleteFile(fileName);
    }
}
