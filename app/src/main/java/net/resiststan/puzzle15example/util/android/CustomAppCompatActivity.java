package net.resiststan.puzzle15example.util.android;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;

import com.resiststan.puzzle15example.R;

import java.util.Locale;
import java.util.Objects;

public abstract class CustomAppCompatActivity extends AppCompatActivity {
    protected final String TAG = this.getClass().getSimpleName();

    private SharedPreferences sp;
    private String currentLanguage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        setTheme(sp);
        setLanguage(sp);
        currentLanguage = getLanguage();

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        setOrientation(sp);

        super.onStart();
    }

    @Override
    protected void onResume() {
        if (!currentLanguage.equals(getLanguage())) {
            recreate();
        }

        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        recreate();

        super.onConfigurationChanged(newConfig);
    }

    protected void setOrientation(SharedPreferences sp) {
        boolean isHoldOrientation = sp.getBoolean(getResources().getString(R.string.pref_key_orientation), false);

        if (isHoldOrientation) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
    }

    protected void setTheme(SharedPreferences sp) {
        String[] themeCodes = getResources().getStringArray(R.array.pref_theme_values);
        String theme = sp.getString(getResources().getString(R.string.pref_key_theme), themeCodes[0]);

        if (Objects.equals(theme, themeCodes[1])) {
            setTheme(R.style.AppThemeCustomDark);
        } else {
            setTheme(R.style.AppThemeCustomLight);
        }
    }

    protected void setLanguage(SharedPreferences sp) {
        String language = getLanguage();

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
    }

    private String getLanguage() {
        String[] languageCode = getResources().getStringArray(R.array.pref_language_entries);
        return sp.getString(getResources().getString(R.string.pref_key_language), languageCode[0]);
    }
}
