package net.resiststan.puzzle15example.util.android;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public abstract class MyOnTouchListener implements View.OnTouchListener {

    private final GestureDetector gestureDetector;

    public MyOnTouchListener(Context context) {
        gestureDetector = new GestureDetector(context, new GestureListener());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onFling(MotionEvent downEvent, MotionEvent moveEvent, float velocityX, float velocityY) {
            boolean result = false;
            float diffY = moveEvent.getY() - downEvent.getY();
            float diffX = moveEvent.getX() - downEvent.getX();

            //define what move is great, Y or X
            if (Math.abs(diffX) > Math.abs(diffY)) {
                // left or right
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        onLeftToRightSwipe();
                    } else {
                        onRightToLeftSwipe();
                    }

                    result = true;
                }
            } else {
                // up or down
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onTopToBottomSwipe();
                    } else {
                        onBottomToTopSwipe();
                    }

                    result = true;
                }
            }

            return result;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return super.onDown(e);
//            return true;
        }

        @Override
        public boolean onContextClick(MotionEvent e) {
            return super.onContextClick(e);
        }
    }

    public abstract void onRightToLeftSwipe();

    public abstract void onLeftToRightSwipe();

    public abstract void onTopToBottomSwipe();

    public abstract void onBottomToTopSwipe();
}
