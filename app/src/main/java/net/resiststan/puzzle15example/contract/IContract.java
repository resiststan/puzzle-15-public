package net.resiststan.puzzle15example.contract;

import android.content.Context;

public interface IContract {

    interface IGame {
        interface IPresenter {
            void attachView(IView view);

            void viewIsReady();

            void startNewGame();

            void moveCell(int cellId);

            void onRightToLeftSwipe();

            void onLeftToRightSwipe();

            void onTopToBottomSwipe();

            void onBottomToTopSwipe();

            int getFieldSide();

            String getFieldType();
        }

        interface IView {
            void clearCell(int ind);

            void fillCell(int ind, String value);

            void gameCompleted();

            Context getContext();

            void setScoreboard(int best, int current);

            void updateScoreboard(int n);
        }
    }
}
